import { NegociacoesView, MensagemView } from '../views/index';
import { Negociacao, Negociacoes } from '../models/index';

import { domInject, debounce } from '../helpers/decorators/index';
import { NegociacaoService } from '../services/index';

import { imprime } from '../helpers/index';

let timer: any = 0;

export class NegociacaoController {

  @domInject('#data')
  private _inputData: JQuery;

  @domInject('#quantidade')
  private _inputQuantidade: JQuery;

  @domInject('#valor')
  private _inputValor: JQuery;

  private _negociacoes = new Negociacoes();
  private _negociacoesView = new NegociacoesView('#negociacoesView');
  private _mensagemView = new MensagemView('#mensagemView');

  private _service = new NegociacaoService();

  constructor() {

    this._negociacoesView.update(this._negociacoes);
  }

  @debounce()
  adiciona(event: Event) {
    let data = new Date(this._inputData.val().replace(/-/g, ','));

    if (!this._ehDiaUtil(data)) {
      this._mensagemView.update('Não é possivel inserir negociações durante finais de semana.');
      return;
    }

    const negociacao = new Negociacao(
      data,
      parseInt(this._inputQuantidade.val()),
      parseFloat(this._inputValor.val())
    )

    this._negociacoes.adiciona(negociacao);

    imprime(negociacao, this._negociacoes);

    this._negociacoesView.update(this._negociacoes);
    this._mensagemView.update('Negociação adicionada com sucesso.');
  }

  private _ehDiaUtil(data: Date) {
    return data.getDay() !== DiaDaSemana.Sabado && data.getDay() !== DiaDaSemana.Domingo;
  }

  @debounce()
  async importaDados() {
    try {
      const negociacoesParaImportar = await this._service.obterNegociacoes(
        res => {
          if (res.ok) return res;
          throw new Error(res.statusText);
        }
      );

      const negociacoesJaImportadas = this._negociacoes.paraArray();

      negociacoesParaImportar
        .filter(negociacao => (
          !negociacoesJaImportadas.some(
            jaImportada => negociacao.ehIgual(jaImportada)
          )
        ))
        .forEach(negociacao => this._negociacoes.adiciona(negociacao))

      this._negociacoesView.update(this._negociacoes);
    } catch (err) {
      this._mensagemView.update(err.message);
    }
  }
}

enum DiaDaSemana {

  Domingo,
  Segunda,
  Terca,
  Quarta,
  Quinta,
  Sexta,
  Sabado,
}
